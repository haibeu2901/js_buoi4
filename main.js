// Exercise 1
function arrangeNumber() {
  var number1Value = document.getElementById("number1-1").value * 1;
  var number2Value = document.getElementById("number2-1").value * 1;
  var number3Value = document.getElementById("number3-1").value * 1;
  if (number1Value <= number2Value && number1Value <= number3Value) {
    if (number2Value <= number3Value) {
      document.getElementById(
        "result1"
      ).innerHTML = `<p>${number1Value} ${number2Value} ${number3Value}</p>`;
    } else {
      document.getElementById(
        "result1"
      ).innerHTML = `<p>${number1Value} ${number3Value} ${number2Value}</p>`;
    }
  } else if (number2Value <= number1Value && number2Value <= number3Value) {
    if (number1Value <= number3Value) {
      document.getElementById(
        "result1"
      ).innerHTML = `<p>${number2Value} ${number1Value} ${number3Value}</p>`;
    } else {
      document.getElementById(
        "result1"
      ).innerHTML = `<p>${number2Value} ${number3Value} ${number1Value}</p>`;
    }
  } else {
    if (number1Value <= number2Value) {
      document.getElementById(
        "result1"
      ).innerHTML = `<p>${number3Value} ${number1Value} ${number2Value}</p>`;
    } else {
      document.getElementById(
        "result1"
      ).innerHTML = `<p>${number3Value} ${number2Value} ${number1Value}</p>`;
    }
  }
}

// Exercise 2
function sayHi() {
  var famNameValue = document.getElementById("familyName").value;
  switch (famNameValue) {
    case "dad":
      document.getElementById("result2").innerHTML = `<p>Xin chào Bố!</p>`;
      break;
    case "mom":
      document.getElementById("result2").innerHTML = `<p>Xin chào Mẹ!</p>`;
      break;
    case "bro":
      document.getElementById(
        "result2"
      ).innerHTML = `<p>Xin chào Anh trai!</p>`;
      break;
    default:
      document.getElementById("result2").innerHTML = `<p>Xin chào Em gái!</p>`;
      break;
  }
}

// Exercise 3
function countNum() {
  const totalNumber = 3;
  var even = 0;
  var number1Value = document.getElementById("number1-3").value * 1;
  var number2Value = document.getElementById("number2-3").value * 1;
  var number3Value = document.getElementById("number3-3").value * 1;
  if (number1Value % 2 == 0) {
    even++;
  }
  if (number2Value % 2 == 0) {
    even++;
  }
  if (number3Value % 2 == 0) {
    even++;
  }
  var odd = totalNumber - even;
  document.getElementById(
    "result3"
  ).innerHTML = `<p>Có ${even} số chẵn, ${odd} số lẻ</p>`;
}

// Exercise 4
function guessTriangle() {
  var edge1Value = document.getElementById("number1-4").value * 1;
  var edge2Value = document.getElementById("number2-4").value * 1;
  var edge3Value = document.getElementById("number3-4").value * 1;
  var sqEdge1 = edge1Value * edge1Value;
  var sqEdge2 = edge2Value * edge2Value;
  var sqEdge3 = edge3Value * edge3Value;
  if (edge1Value == edge2Value) {
    if (edge2Value == edge3Value) {
      document.getElementById("result4").innerHTML = `<p>đều</p>`;
    } else {
      document.getElementById("result4").innerHTML = `<p>cân</p>`;
    }
  } else if (edge1Value == edge3Value) {
    if (edge2Value == edge3Value) {
      document.getElementById("result4").innerHTML = `<p>đều</p>`;
    } else {
      document.getElementById("result4").innerHTML = `<p>cân</p>`;
    }
  } else if (edge2Value == edge3Value) {
    if (edge1Value == edge2Value) {
      document.getElementById("result4").innerHTML = `<p>đều</p>`;
    } else {
      document.getElementById("result4").innerHTML = `<p>cân</p>`;
    }
  } else if (edge1Value == Math.floor(Math.sqrt(sqEdge2 + sqEdge3))) {
    document.getElementById("result4").innerHTML = `<p>vuông</p>`;
  } else if (edge2Value == Math.floor(Math.sqrt(sqEdge1 + sqEdge3))) {
    document.getElementById("result4").innerHTML = `<p>vuông</p>`;
  } else if (edge3Value == Math.floor(Math.sqrt(sqEdge1 + sqEdge2))) {
    document.getElementById("result4").innerHTML = `<p>vuông</p>`;
  } else {
    document.getElementById("result4").innerHTML = `<p>loại khác</p>`;
  }
}

// Exercise 5
function prevDay() {
  var dayValue = document.getElementById("number1-5").value * 1;
  var monthValue = document.getElementById("number2-5").value * 1;
  var yearValue = document.getElementById("number3-5").value * 1;
  var preDayValue;
  var preMonthValue;
  var preYearValue;
  if (dayValue == 1) {
    if (monthValue == 1) {
      preDayValue = 31;
      preMonthValue = 12;
      preYearValue = yearValue - 1;
    } else if (monthValue == 3) {
      preDayValue = 28;
      preMonthValue = monthValue - 1;
      preYearValue = yearValue;
    } else if (
      monthValue == 2 ||
      monthValue == 4 ||
      monthValue == 6 ||
      monthValue == 9 ||
      monthValue == 11
    ) {
      preDayValue = 31;
      preMonthValue = monthValue - 1;
      preYearValue = yearValue;
    } else {
      preDayValue = 30;
      preMonthValue = monthValue - 1;
      preYearValue = yearValue;
    }
  } else {
    preDayValue = dayValue - 1;
    preMonthValue = monthValue;
    preYearValue = yearValue;
  }
  document.getElementById(
    "result5-1"
  ).innerHTML = `<p>${preDayValue}/${preMonthValue}/${preYearValue}</p>`;
}

function nextDay() {
  var dayValue = document.getElementById("number1-5").value * 1;
  var monthValue = document.getElementById("number2-5").value * 1;
  var yearValue = document.getElementById("number3-5").value * 1;
  var nextDayValue;
  var nextMonthValue;
  var nextYearValue;
  if (dayValue == 31) {
    if (monthValue == 12) {
      nextDayValue = 1;
      nextMonthValue = 1;
      nextYearValue = yearValue + 1;
    } else if (
      monthValue == 1 ||
      monthValue == 3 ||
      monthValue == 5 ||
      monthValue == 7 ||
      monthValue == 8 ||
      monthValue == 10
    ) {
      nextDayValue = 1;
      nextMonthValue = monthValue + 1;
      nextYearValue = yearValue;
    }
  } else if (dayValue == 30) {
    if (
      monthValue == 4 ||
      monthValue == 6 ||
      monthValue == 9 ||
      monthValue == 11
    ) {
      nextDayValue = 1;
      nextMonthValue = monthValue + 1;
      nextYearValue = yearValue;
    } else {
      nextDayValue = dayValue + 1;
      nextMonthValue = monthValue;
      nextYearValue = yearValue;
    }
  } else if (dayValue == 28) {
    if (monthValue == 2) {
      nextDayValue = 1;
      nextMonthValue = 3;
      nextYearValue = yearValue;
    } else {
      nextDayValue = dayValue + 1;
      nextMonthValue = monthValue;
      nextYearValue = yearValue;
    }
  } else {
    nextDayValue = dayValue + 1;
    nextMonthValue = monthValue;
    nextYearValue = yearValue;
  }
  document.getElementById(
    "result5-2"
  ).innerHTML = `<p>${nextDayValue}/${nextMonthValue}/${nextYearValue}</p>`;
}

// Exercise 6
function countDay() {
  var monthValue = document.getElementById("number1-6").value * 1;
  var yearValue = document.getElementById("number2-6").value * 1;
  var dayValue;
  if (
    monthValue == 1 ||
    monthValue == 3 ||
    monthValue == 5 ||
    monthValue == 7 ||
    monthValue == 8 ||
    monthValue == 10 ||
    monthValue == 12
  ) {
    dayValue = 31;
  } else if (
    monthValue == 4 ||
    monthValue == 6 ||
    monthValue == 9 ||
    monthValue == 11
  ) {
    dayValue = 30;
  } else {
    if (yearValue % 2 == 1) {
      dayValue = 28;
    } else {
      if (yearValue % 400 == 0) {
        dayValue = 29;
      } else if (yearValue % 4 == 0 && yearValue % 100 != 0) {
        dayValue = 29;
      } else {
        dayValue = 28;
      }
    }
  }
  document.getElementById(
    "result6"
  ).innerHTML = `<p>Tháng ${monthValue} năm ${yearValue} có ${dayValue} ngày</p>`;
}

// Exercise 7
function readNumber() {
  var numberValue = document.getElementById("number1-7").value * 1;
  var units = numberValue % 10;
  numberValue = Math.floor(numberValue / 10);
  var dozens = numberValue % 10;
  numberValue = Math.floor(numberValue / 10);
  var hundreds = numberValue;
  if (dozens == 0) {
    if (units == 0) {
      switch (hundreds) {
        case 1:
          document.getElementById("result7").innerHTML = `<p>Một trăm</p>`;
          break;
        case 2:
          document.getElementById("result7").innerHTML = `<p>Hai trăm</p>`;
          break;
        case 3:
          document.getElementById("result7").innerHTML = `<p>Ba trăm</p>`;
          break;
        case 4:
          document.getElementById("result7").innerHTML = `<p>Bốn trăm</p>`;
          break;
        case 5:
          document.getElementById("result7").innerHTML = `<p>Năm trăm</p>`;
          break;
        case 6:
          document.getElementById("result7").innerHTML = `<p>Sáu trăm</p>`;
          break;
        case 7:
          document.getElementById("result7").innerHTML = `<p>Bảy trăm</p>`;
          break;
        case 8:
          document.getElementById("result7").innerHTML = `<p>Tám trăm</p>`;
          break;
        default:
          document.getElementById("result7").innerHTML = `<p>Chín trăm</p>`;
          break;
      }
    } else {
      switch (units) {
        case 1:
          switch (hundreds) {
            case 1:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Một trăm linh một</p>`;
              break;
            case 2:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Hai trăm linh một</p>`;
              break;
            case 3:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Ba trăm linh một</p>`;
              break;
            case 4:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bốn trăm linh một</p>`;
              break;
            case 5:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Năm trăm linh một</p>`;
              break;
            case 6:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Sáu trăm linh một</p>`;
              break;
            case 7:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bảy trăm linh một</p>`;
              break;
            case 8:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Tám trăm linh một</p>`;
              break;
            default:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Chín trăm linh một</p>`;
              break;
          }
          break;
        case 2:
          switch (hundreds) {
            case 1:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Một trăm linh hai</p>`;
              break;
            case 2:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Hai trăm linh hai</p>`;
              break;
            case 3:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Ba trăm linh hai</p>`;
              break;
            case 4:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bốn trăm linh hai</p>`;
              break;
            case 5:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Năm trăm linh hai</p>`;
              break;
            case 6:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Sáu trăm linh hai</p>`;
              break;
            case 7:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bảy trăm linh hai</p>`;
              break;
            case 8:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Tám trăm linh hai</p>`;
              break;
            default:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Chín trăm linh hai</p>`;
              break;
          }
          break;
        case 3:
          switch (hundreds) {
            case 1:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Một trăm linh ba</p>`;
              break;
            case 2:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Hai trăm linh ba</p>`;
              break;
            case 3:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Ba trăm linh ba</p>`;
              break;
            case 4:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bốn trăm linh ba</p>`;
              break;
            case 5:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Năm trăm linh ba</p>`;
              break;
            case 6:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Sáu trăm linh ba</p>`;
              break;
            case 7:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bảy trăm linh ba</p>`;
              break;
            case 8:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Tám trăm linh ba</p>`;
              break;
            default:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Chín trăm linh ba</p>`;
              break;
          }
          break;
        case 4:
          switch (hundreds) {
            case 1:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Một trăm linh bốn</p>`;
              break;
            case 2:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Hai trăm linh bốn</p>`;
              break;
            case 3:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Ba trăm linh bốn</p>`;
              break;
            case 4:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bốn trăm linh bốn</p>`;
              break;
            case 5:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Năm trăm linh bốn</p>`;
              break;
            case 6:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Sáu trăm linh bốn</p>`;
              break;
            case 7:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bảy trăm linh bốn</p>`;
              break;
            case 8:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Tám trăm linh bốn</p>`;
              break;
            default:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Chín trăm linh bốn</p>`;
              break;
          }
          break;
        case 5:
          switch (hundreds) {
            case 1:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Một trăm linh năm</p>`;
              break;
            case 2:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Hai trăm linh năm</p>`;
              break;
            case 3:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Ba trăm linh năm</p>`;
              break;
            case 4:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bốn trăm linh năm</p>`;
              break;
            case 5:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Năm trăm linh năm</p>`;
              break;
            case 6:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Sáu trăm linh năm</p>`;
              break;
            case 7:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bảy trăm linh năm</p>`;
              break;
            case 8:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Tám trăm linh năm</p>`;
              break;
            default:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Chín trăm linh năm</p>`;
              break;
          }
          break;
        case 6:
          switch (hundreds) {
            case 1:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Một trăm linh sáu</p>`;
              break;
            case 2:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Hai trăm linh sáu</p>`;
              break;
            case 3:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Ba trăm linh sáu</p>`;
              break;
            case 4:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bốn trăm linh sáu</p>`;
              break;
            case 5:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Năm trăm linh sáu</p>`;
              break;
            case 6:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Sáu trăm linh sáu</p>`;
              break;
            case 7:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bảy trăm linh sáu</p>`;
              break;
            case 8:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Tám trăm linh sáu</p>`;
              break;
            default:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Chín trăm linh sáu</p>`;
              break;
          }
          break;
        case 7:
          switch (hundreds) {
            case 1:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Một trăm linh bảy</p>`;
              break;
            case 2:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Hai trăm linh bảy</p>`;
              break;
            case 3:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Ba trăm linh bảy</p>`;
              break;
            case 4:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bốn trăm linh bảy</p>`;
              break;
            case 5:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Năm trăm linh bảy</p>`;
              break;
            case 6:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Sáu trăm linh bảy</p>`;
              break;
            case 7:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bảy trăm linh bảy</p>`;
              break;
            case 8:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Tám trăm linh bảy</p>`;
              break;
            default:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Chín trăm linh bảy</p>`;
              break;
          }
          break;
        case 8:
          switch (hundreds) {
            case 1:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Một trăm linh tám</p>`;
              break;
            case 2:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Hai trăm linh tám</p>`;
              break;
            case 3:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Ba trăm linh tám</p>`;
              break;
            case 4:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bốn trăm linh tám</p>`;
              break;
            case 5:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Năm trăm linh tám</p>`;
              break;
            case 6:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Sáu trăm linh tám</p>`;
              break;
            case 7:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bảy trăm linh tám</p>`;
              break;
            case 8:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Tám trăm linh tám</p>`;
              break;
            default:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Chín trăm linh tám</p>`;
              break;
          }
          break;
        default:
          switch (hundreds) {
            case 1:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Một trăm linh chín</p>`;
              break;
            case 2:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Hai trăm linh chín</p>`;
              break;
            case 3:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Ba trăm linh chín</p>`;
              break;
            case 4:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bốn trăm linh chín</p>`;
              break;
            case 5:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Năm trăm linh chín</p>`;
              break;
            case 6:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Sáu trăm linh chín</p>`;
              break;
            case 7:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bảy trăm linh chín</p>`;
              break;
            case 8:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Tám trăm linh chín</p>`;
              break;
            default:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Chín trăm linh chín</p>`;
              break;
          }
          break;
      }
    }
  } else {
    switch (dozens) {
      case 1:
        if (units == 0) {
          switch (hundreds) {
            case 1:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Một trăm mười</p>`;
              break;
            case 2:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Hai trăm mười</p>`;
              break;
            case 3:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Ba trăm mười</p>`;
              break;
            case 4:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bốn trăm mười</p>`;
              break;
            case 5:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Năm trăm mười</p>`;
              break;
            case 6:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Sáu trăm mười</p>`;
              break;
            case 7:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bảy trăm mười</p>`;
              break;
            case 8:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Tám trăm mười</p>`;
              break;
            default:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Chín trăm mười</p>`;
              break;
          }
        } else {
          switch (units) {
            case 1:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm mười một</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm mười một</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm mười một</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm mười một</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm mười một</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm mười một</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm mười một</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm mười một</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm mười một</p>`;
                  break;
              }
              break;
            case 2:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm mười hai</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm mười hai</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm mười hai</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm mười hai</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm mười hai</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm mười hai</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm mười hai</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm mười hai</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm mười hai</p>`;
                  break;
              }
              break;
            case 3:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm mười ba</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm mười ba</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm mười ba</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm mười ba</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm mười ba</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm mười ba</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm mười ba</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm mười ba</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm mười ba</p>`;
                  break;
              }
              break;
            case 4:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm mười bốn</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm mười bốn</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm mười bốn</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm mười bốn</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm mười bốn</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm mười bốn</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm mười bốn</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm mười bốn</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm mười bốn</p>`;
                  break;
              }
              break;
            case 5:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm mười lăm</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm mười lăm</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm mười lăm</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm mười lăm</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm mười lăm</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm mười lăm</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm mười lăm</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm mười lăm</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm mười lăm</p>`;
                  break;
              }
              break;
            case 6:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm mười sáu</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm mười sáu</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm mười sáu</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm mười sáu</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm mười sáu</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm mười sáu</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm mười sáu</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm mười sáu</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm mười sáu</p>`;
                  break;
              }
              break;
            case 7:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm mười bảy</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm mười bảy</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm mười bảy</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm mười bảy</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm mười bảy</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm mười bảy</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm mười bảy</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm mười bảy</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm mười bảy</p>`;
                  break;
              }
              break;
            case 8:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm mười tám</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm mười tám</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm mười tám</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm mười tám</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm mười tám</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm mười tám</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm mười tám</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm mười tám</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm mười tám</p>`;
                  break;
              }
              break;
            default:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm mười chín</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm mười chín</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm mười chín</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm mười chín</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm mười chín</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm mười chín</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm mười chín</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm mười chín</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm mười chín</p>`;
                  break;
              }
              break;
          }
        }
        break;
      case 2:
        if (units == 0) {
          switch (hundreds) {
            case 1:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Một trăm hai mươi</p>`;
              break;
            case 2:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Hai trăm hai mươi</p>`;
              break;
            case 3:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Ba trăm hai mươi</p>`;
              break;
            case 4:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bốn trăm hai mươi</p>`;
              break;
            case 5:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Năm trăm hai mươi</p>`;
              break;
            case 6:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Sáu trăm hai mươi</p>`;
              break;
            case 7:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bảy trăm hai mươi</p>`;
              break;
            case 8:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Tám trăm hai mươi</p>`;
              break;
            default:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Chín trăm hai mươi</p>`;
              break;
          }
        } else {
          switch (units) {
            case 1:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm hai mươi mốt</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm hai mươi mốt</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm hai mươi mốt</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm hai mươi mốt</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm hai mươi mốt</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm hai mươi mốt</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm hai mươi mốt</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm hai mươi mốt</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm hai mươi mốt</p>`;
                  break;
              }
              break;
            case 2:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm hai mươi hai</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm hai mươi hai</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm hai mươi hai</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm hai mươi hai</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm hai mươi hai</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm hai mươi hai</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm hai mươi hai</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm hai mươi hai</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm hai mươi hai</p>`;
                  break;
              }
              break;
            case 3:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm hai mươi ba</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm hai mươi ba</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm hai mươi ba</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm hai mươi ba</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm hai mươi ba</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm hai mươi ba</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm hai mươi ba</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm hai mươi ba</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm hai mươi ba</p>`;
                  break;
              }
              break;
            case 4:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm hai mươi tư</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm hai mươi tư</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm hai mươi tư</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm hai mươi tư</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm hai mươi tư</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm hai mươi tư</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm hai mươi tư</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm hai mươi tư</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm hai mươi tư</p>`;
                  break;
              }
              break;
            case 5:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm hai mươi lăm</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm hai mươi lăm</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm hai mươi lăm</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm hai mươi lăm</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm hai mươi lăm</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm hai mươi lăm</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm hai mươi lăm</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm hai mươi lăm</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm hai mươi lăm</p>`;
                  break;
              }
              break;
            case 6:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm hai mươi sáu</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm hai mươi sáu</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm hai mươi sáu</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm hai mươi sáu</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm hai mươi sáu</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm hai mươi sáu</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm hai mươi sáu</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm hai mươi sáu</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm hai mươi sáu</p>`;
                  break;
              }
              break;
            case 7:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm hai mươi bảy</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm hai mươi bảy</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm hai mươi bảy</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm hai mươi bảy</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm hai mươi bảy</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm hai mươi bảy</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm hai mươi bảy</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm hai mươi bảy</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm hai mươi bảy</p>`;
                  break;
              }
              break;
            case 8:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm hai mươi tám</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm hai mươi tám</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm hai mươi tám</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm hai mươi tám</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm hai mươi tám</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm hai mươi tám</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm hai mươi tám</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm hai mươi tám</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm hai mươi tám</p>`;
                  break;
              }
              break;
            default:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm hai mươi chín</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm hai mươi chín</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm hai mươi chín</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm hai mươi chín</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm hai mươi chín</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm hai mươi chín</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm hai mươi chín</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm hai mươi chín</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm hai mươi chín</p>`;
                  break;
              }
              break;
          }
        }
        break;
      case 3:
        if (units == 0) {
          switch (hundreds) {
            case 1:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Một trăm ba mươi</p>`;
              break;
            case 2:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Hai trăm ba mươi</p>`;
              break;
            case 3:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Ba trăm ba mươi</p>`;
              break;
            case 4:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bốn trăm ba mươi</p>`;
              break;
            case 5:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Năm trăm ba mươi</p>`;
              break;
            case 6:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Sáu trăm ba mươi</p>`;
              break;
            case 7:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bảy trăm ba mươi</p>`;
              break;
            case 8:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Tám trăm ba mươi</p>`;
              break;
            default:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Chín trăm ba mươi</p>`;
              break;
          }
        } else {
          switch (units) {
            case 1:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm ba mươi mốt</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm ba mươi mốt</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm ba mươi mốt</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm ba mươi mốt</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm ba mươi mốt</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm ba mươi mốt</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm ba mươi mốt</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm ba mươi mốt</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm ba mươi mốt</p>`;
                  break;
              }
              break;
            case 2:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm ba mươi hai</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm ba mươi hai</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm ba mươi hai</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm ba mươi hai</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm ba mươi hai</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm ba mươi hai</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm ba mươi hai</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm ba mươi hai</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm ba mươi hai</p>`;
                  break;
              }
              break;
            case 3:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm ba mươi ba</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm ba mươi ba</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm ba mươi ba</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm ba mươi ba</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm ba mươi ba</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm ba mươi ba</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm ba mươi ba</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm ba mươi ba</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm ba mươi ba</p>`;
                  break;
              }
              break;
            case 4:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm ba mươi tư</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm ba mươi tư</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm ba mươi tư</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm ba mươi tư</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm ba mươi tư</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm ba mươi tư</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm ba mươi tư</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm ba mươi tư</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm ba mươi tư</p>`;
                  break;
              }
              break;
            case 5:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm ba mươi lăm</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm ba mươi lăm</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm ba mươi lăm</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm ba mươi lăm</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm ba mươi lăm</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm ba mươi lăm</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm ba mươi lăm</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm ba mươi lăm</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm ba mươi lăm</p>`;
                  break;
              }
              break;
            case 6:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm ba mươi sáu</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm ba mươi sáu</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm ba mươi sáu</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm ba mươi sáu</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm ba mươi sáu</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm ba mươi sáu</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm ba mươi sáu</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm ba mươi sáu</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm ba mươi sáu</p>`;
                  break;
              }
              break;
            case 7:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm ba mươi bảy</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm ba mươi bảy</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm ba mươi bảy</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm ba mươi bảy</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm ba mươi bảy</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm ba mươi bảy</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm ba mươi bảy</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm ba mươi bảy</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm ba mươi bảy</p>`;
                  break;
              }
              break;
            case 8:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm ba mươi tám</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm ba mươi tám</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm ba mươi tám</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm ba mươi tám</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm ba mươi tám</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm ba mươi tám</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm ba mươi tám</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm ba mươi tám</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm ba mươi tám</p>`;
                  break;
              }
              break;
            default:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm ba mươi chín</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm ba mươi chín</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm ba mươi chín</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm ba mươi chín</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm ba mươi chín</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm ba mươi chín</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm ba mươi chín</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm ba mươi chín</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm ba mươi chín</p>`;
                  break;
              }
              break;
          }
        }
        break;
      case 4:
        if (units == 0) {
          switch (hundreds) {
            case 1:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Một trăm bốn mươi</p>`;
              break;
            case 2:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Hai trăm bốn mươi</p>`;
              break;
            case 3:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Ba trăm bốn mươi</p>`;
              break;
            case 4:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bốn trăm bốn mươi</p>`;
              break;
            case 5:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Năm trăm bốn mươi</p>`;
              break;
            case 6:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Sáu trăm bốn mươi</p>`;
              break;
            case 7:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bảy trăm bốn mươi</p>`;
              break;
            case 8:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Tám trăm bốn mươi</p>`;
              break;
            default:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Chín trăm bốn mươi</p>`;
              break;
          }
        } else {
          switch (units) {
            case 1:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm bốn mươi mốt</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm bốn mươi mốt</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm bốn mươi mốt</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm bốn mươi mốt</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm bốn mươi mốt</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm bốn mươi mốt</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm bốn mươi mốt</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm bốn mươi mốt</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm bốn mươi mốt</p>`;
                  break;
              }
              break;
            case 2:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm bốn mươi hai</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm bốn mươi hai</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm bốn mươi hai</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm bốn mươi hai</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm bốn mươi hai</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm bốn mươi hai</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm bốn mươi hai</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm bốn mươi hai</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm bốn mươi hai</p>`;
                  break;
              }
              break;
            case 3:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm bốn mươi ba</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm bốn mươi ba</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm bốn mươi ba</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm bốn mươi ba</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm bốn mươi ba</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm bốn mươi ba</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm bốn mươi ba</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm bốn mươi ba</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm bốn mươi ba</p>`;
                  break;
              }
              break;
            case 4:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm bốn mươi tư</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm bốn mươi tư</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm bốn mươi tư</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm bốn mươi tư</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm bốn mươi tư</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm bốn mươi tư</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm bốn mươi tư</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm bốn mươi tư</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm bốn mươi tư</p>`;
                  break;
              }
              break;
            case 5:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm bốn mươi lăm</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm bốn mươi lăm</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm bốn mươi lăm</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm bốn mươi lăm</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm bốn mươi lăm</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm bốn mươi lăm</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm bốn mươi lăm</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm bốn mươi lăm</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm bốn mươi lăm</p>`;
                  break;
              }
              break;
            case 6:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm bốn mươi sáu</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm bốn mươi sáu</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm bốn mươi sáu</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm bốn mươi sáu</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm bốn mươi sáu</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm bốn mươi sáu</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm bốn mươi sáu</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm bốn mươi sáu</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm bốn mươi sáu</p>`;
                  break;
              }
              break;
            case 7:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm bốn mươi bảy</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm bốn mươi bảy</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm bốn mươi bảy</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm bốn mươi bảy</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm bốn mươi bảy</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm bốn mươi bảy</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm bốn mươi bảy</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm bốn mươi bảy</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm bốn mươi bảy</p>`;
                  break;
              }
              break;
            case 8:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm bốn mươi tám</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm bốn mươi tám</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm bốn mươi tám</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm bốn mươi tám</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm bốn mươi tám</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm bốn mươi tám</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm bốn mươi tám</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm bốn mươi tám</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm bốn mươi tám</p>`;
                  break;
              }
              break;
            default:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm bốn mươi chín</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm bốn mươi chín</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm bốn mươi chín</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm bốn mươi chín</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm bốn mươi chín</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm bốn mươi chín</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm bốn mươi chín</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm bốn mươi chín</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm bốn mươi chín</p>`;
                  break;
              }
              break;
          }
        }
        break;
      case 5:
        if (units == 0) {
          switch (hundreds) {
            case 1:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Một trăm năm mươi</p>`;
              break;
            case 2:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Hai trăm năm mươi</p>`;
              break;
            case 3:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Ba trăm năm mươi</p>`;
              break;
            case 4:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bốn trăm năm mươi</p>`;
              break;
            case 5:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Năm trăm năm mươi</p>`;
              break;
            case 6:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Sáu trăm năm mươi</p>`;
              break;
            case 7:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bảy trăm năm mươi</p>`;
              break;
            case 8:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Tám trăm năm mươi</p>`;
              break;
            default:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Chín trăm năm mươi</p>`;
              break;
          }
        } else {
          switch (units) {
            case 1:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm năm mươi mốt</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm năm mươi mốt</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm năm mươi mốt</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm năm mươi mốt</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm năm mươi mốt</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm năm mươi mốt</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm năm mươi mốt</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm năm mươi mốt</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm năm mươi mốt</p>`;
                  break;
              }
              break;
            case 2:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm năm mươi hai</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm năm mươi hai</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm năm mươi hai</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm năm mươi hai</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm năm mươi hai</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm năm mươi hai</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm năm mươi hai</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm năm mươi hai</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm năm mươi hai</p>`;
                  break;
              }
              break;
            case 3:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm năm mươi ba</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm năm mươi ba</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm năm mươi ba</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm năm mươi ba</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm năm mươi ba</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm năm mươi ba</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm năm mươi ba</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm năm mươi ba</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm năm mươi ba</p>`;
                  break;
              }
              break;
            case 4:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm năm mươi tư</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm năm mươi tư</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm năm mươi tư</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm năm mươi tư</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm năm mươi tư</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm năm mươi tư</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm năm mươi tư</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm năm mươi tư</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm năm mươi tư</p>`;
                  break;
              }
              break;
            case 5:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm năm mươi lăm</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm năm mươi lăm</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm năm mươi lăm</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm năm mươi lăm</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm năm mươi lăm</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm năm mươi lăm</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm năm mươi lăm</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm năm mươi lăm</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm năm mươi lăm</p>`;
                  break;
              }
              break;
            case 6:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm năm mươi sáu</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm năm mươi sáu</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm năm mươi sáu</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm năm mươi sáu</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm năm mươi sáu</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm năm mươi sáu</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm năm mươi sáu</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm năm mươi sáu</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm năm mươi sáu</p>`;
                  break;
              }
              break;
            case 7:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm năm mươi bảy</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm năm mươi bảy</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm năm mươi bảy</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm năm mươi bảy</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm năm mươi bảy</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm năm mươi bảy</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm năm mươi bảy</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm năm mươi bảy</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm năm mươi bảy</p>`;
                  break;
              }
              break;
            case 8:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm năm mươi tám</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm năm mươi tám</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm năm mươi tám</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm năm mươi tám</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm năm mươi tám</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm năm mươi tám</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm năm mươi tám</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm năm mươi tám</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm năm mươi tám</p>`;
                  break;
              }
              break;
            default:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm năm mươi chín</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm năm mươi chín</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm năm mươi chín</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm năm mươi chín</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm năm mươi chín</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm năm mươi chín</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm năm mươi chín</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm năm mươi chín</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm năm mươi chín</p>`;
                  break;
              }
              break;
          }
        }
        break;
      case 6:
        if (units == 0) {
          switch (hundreds) {
            case 1:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Một trăm sáu mươi</p>`;
              break;
            case 2:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Hai trăm sáu mươi</p>`;
              break;
            case 3:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Ba trăm sáu mươi</p>`;
              break;
            case 4:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bốn trăm sáu mươi</p>`;
              break;
            case 5:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Năm trăm sáu mươi</p>`;
              break;
            case 6:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Sáu trăm sáu mươi</p>`;
              break;
            case 7:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bảy trăm sáu mươi</p>`;
              break;
            case 8:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Tám trăm sáu mươi</p>`;
              break;
            default:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Chín trăm sáu mươi</p>`;
              break;
          }
        } else {
          switch (units) {
            case 1:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm sáu mươi mốt</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm sáu mươi mốt</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm sáu mươi mốt</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm sáu mươi mốt</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm sáu mươi mốt</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm sáu mươi mốt</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm sáu mươi mốt</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm sáu mươi mốt</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm sáu mươi mốt</p>`;
                  break;
              }
              break;
            case 2:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm sáu mươi hai</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm sáu mươi hai</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm sáu mươi hai</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm sáu mươi hai</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm sáu mươi hai</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm sáu mươi hai</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm sáu mươi hai</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm sáu mươi hai</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm sáu mươi hai</p>`;
                  break;
              }
              break;
            case 3:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm sáu mươi ba</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm sáu mươi ba</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm sáu mươi ba</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm sáu mươi ba</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm sáu mươi ba</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm sáu mươi ba</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm sáu mươi ba</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm sáu mươi ba</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm sáu mươi ba</p>`;
                  break;
              }
              break;
            case 4:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm sáu mươi tư</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm sáu mươi tư</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm sáu mươi tư</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm sáu mươi tư</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm sáu mươi tư</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm sáu mươi tư</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm sáu mươi tư</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm sáu mươi tư</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm sáu mươi tư</p>`;
                  break;
              }
              break;
            case 5:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm sáu mươi lăm</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm sáu mươi lăm</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm sáu mươi lăm</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm sáu mươi lăm</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm sáu mươi lăm</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm sáu mươi lăm</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm sáu mươi lăm</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm sáu mươi lăm</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm sáu mươi lăm</p>`;
                  break;
              }
              break;
            case 6:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm sáu mươi sáu</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm sáu mươi sáu</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm sáu mươi sáu</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm sáu mươi sáu</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm sáu mươi sáu</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm sáu mươi sáu</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm sáu mươi sáu</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm sáu mươi sáu</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm sáu mươi sáu</p>`;
                  break;
              }
              break;
            case 7:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm sáu mươi bảy</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm sáu mươi bảy</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm sáu mươi bảy</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm sáu mươi bảy</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm sáu mươi bảy</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm sáu mươi bảy</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm sáu mươi bảy</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm sáu mươi bảy</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm sáu mươi bảy</p>`;
                  break;
              }
              break;
            case 8:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm sáu mươi tám</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm sáu mươi tám</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm sáu mươi tám</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm sáu mươi tám</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm sáu mươi tám</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm sáu mươi tám</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm sáu mươi tám</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm sáu mươi tám</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm sáu mươi tám</p>`;
                  break;
              }
              break;
            default:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm sáu mươi chín</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm sáu mươi chín</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm sáu mươi chín</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm sáu mươi chín</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm sáu mươi chín</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm sáu mươi chín</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm sáu mươi chín</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm sáu mươi chín</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm sáu mươi chín</p>`;
                  break;
              }
              break;
          }
        }
        break;
      case 7:
        if (units == 0) {
          switch (hundreds) {
            case 1:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Một trăm bảy mươi</p>`;
              break;
            case 2:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Hai trăm bảy mươi</p>`;
              break;
            case 3:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Ba trăm bảy mươi</p>`;
              break;
            case 4:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bốn trăm bảy mươi</p>`;
              break;
            case 5:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Năm trăm bảy mươi</p>`;
              break;
            case 6:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Sáu trăm bảy mươi</p>`;
              break;
            case 7:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bảy trăm bảy mươi</p>`;
              break;
            case 8:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Tám trăm bảy mươi</p>`;
              break;
            default:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Chín trăm bảy mươi</p>`;
              break;
          }
        } else {
          switch (units) {
            case 1:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm bảy mươi mốt</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm bảy mươi mốt</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm bảy mươi mốt</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm bảy mươi mốt</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm bảy mươi mốt</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm bảy mươi mốt</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm bảy mươi mốt</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm bảy mươi mốt</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm bảy mươi mốt</p>`;
                  break;
              }
              break;
            case 2:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm bảy mươi hai</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm bảy mươi hai</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm bảy mươi hai</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm bảy mươi hai</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm bảy mươi hai</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm bảy mươi hai</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm bảy mươi hai</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm bảy mươi hai</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm bảy mươi hai</p>`;
                  break;
              }
              break;
            case 3:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm bảy mươi ba</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm bảy mươi ba</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm bảy mươi ba</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm bảy mươi ba</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm bảy mươi ba</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm bảy mươi ba</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm bảy mươi ba</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm bảy mươi ba</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm bảy mươi ba</p>`;
                  break;
              }
              break;
            case 4:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm bảy mươi tư</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm bảy mươi tư</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm bảy mươi tư</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm bảy mươi tư</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm bảy mươi tư</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm bảy mươi tư</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm bảy mươi tư</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm bảy mươi tư</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm bảy mươi tư</p>`;
                  break;
              }
              break;
            case 5:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm bảy mươi lăm</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm bảy mươi lăm</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm bảy mươi lăm</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm bảy mươi lăm</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm bảy mươi lăm</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm bảy mươi lăm</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm bảy mươi lăm</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm bảy mươi lăm</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm bảy mươi lăm</p>`;
                  break;
              }
              break;
            case 6:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm bảy mươi sáu</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm bảy mươi sáu</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm bảy mươi sáu</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm bảy mươi sáu</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm bảy mươi sáu</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm bảy mươi sáu</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm bảy mươi sáu</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm bảy mươi sáu</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm bảy mươi sáu</p>`;
                  break;
              }
              break;
            case 7:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm bảy mươi bảy</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm bảy mươi bảy</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm bảy mươi bảy</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm bảy mươi bảy</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm bảy mươi bảy</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm bảy mươi bảy</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm bảy mươi bảy</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm bảy mươi bảy</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm bảy mươi bảy</p>`;
                  break;
              }
              break;
            case 8:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm bảy mươi tám</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm bảy mươi tám</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm bảy mươi tám</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm bảy mươi tám</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm bảy mươi tám</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm bảy mươi tám</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm bảy mươi tám</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm bảy mươi tám</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm bảy mươi tám</p>`;
                  break;
              }
              break;
            default:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm bảy mươi chín</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm bảy mươi chín</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm bảy mươi chín</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm bảy mươi chín</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm bảy mươi chín</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm bảy mươi chín</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm bảy mươi chín</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm bảy mươi chín</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm bảy mươi chín</p>`;
                  break;
              }
              break;
          }
        }
        break;
      case 8:
        if (units == 0) {
          switch (hundreds) {
            case 1:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Một trăm tám mươi</p>`;
              break;
            case 2:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Hai trăm tám mươi</p>`;
              break;
            case 3:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Ba trăm tám mươi</p>`;
              break;
            case 4:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bốn trăm tám mươi</p>`;
              break;
            case 5:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Năm trăm tám mươi</p>`;
              break;
            case 6:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Sáu trăm tám mươi</p>`;
              break;
            case 7:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bảy trăm tám mươi</p>`;
              break;
            case 8:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Tám trăm tám mươi</p>`;
              break;
            default:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Chín trăm tám mươi</p>`;
              break;
          }
        } else {
          switch (units) {
            case 1:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm tám mươi mốt</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm tám mươi mốt</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm tám mươi mốt</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm tám mươi mốt</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm tám mươi mốt</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm tám mươi mốt</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm tám mươi mốt</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm tám mươi mốt</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm tám mươi mốt</p>`;
                  break;
              }
              break;
            case 2:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm tám mươi hai</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm tám mươi hai</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm tám mươi hai</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm tám mươi hai</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm tám mươi hai</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm tám mươi hai</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm tám mươi hai</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm tám mươi hai</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm tám mươi hai</p>`;
                  break;
              }
              break;
            case 3:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm tám mươi ba</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm tám mươi ba</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm tám mươi ba</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm tám mươi ba</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm tám mươi ba</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm tám mươi ba</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm tám mươi ba</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm tám mươi ba</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm tám mươi ba</p>`;
                  break;
              }
              break;
            case 4:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm tám mươi tư</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm tám mươi tư</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm tám mươi tư</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm tám mươi tư</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm tám mươi tư</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm tám mươi tư</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm tám mươi tư</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm tám mươi tư</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm tám mươi tư</p>`;
                  break;
              }
              break;
            case 5:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm tám mươi lăm</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm tám mươi lăm</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm tám mươi lăm</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm tám mươi lăm</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm tám mươi lăm</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm tám mươi lăm</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm tám mươi lăm</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm tám mươi lăm</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm tám mươi lăm</p>`;
                  break;
              }
              break;
            case 6:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm tám mươi sáu</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm tám mươi sáu</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm tám mươi sáu</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm tám mươi sáu</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm tám mươi sáu</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm tám mươi sáu</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm tám mươi sáu</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm tám mươi sáu</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm tám mươi sáu</p>`;
                  break;
              }
              break;
            case 7:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm tám mươi bảy</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm tám mươi bảy</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm tám mươi bảy</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm tám mươi bảy</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm tám mươi bảy</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm tám mươi bảy</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm tám mươi bảy</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm tám mươi bảy</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm tám mươi bảy</p>`;
                  break;
              }
              break;
            case 8:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm tám mươi tám</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm tám mươi tám</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm tám mươi tám</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm tám mươi tám</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm tám mươi tám</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm tám mươi tám</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm tám mươi tám</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm tám mươi tám</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm tám mươi tám</p>`;
                  break;
              }
              break;
            default:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm tám mươi chín</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm tám mươi chín</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm tám mươi chín</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm tám mươi chín</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm tám mươi chín</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm tám mươi chín</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm tám mươi chín</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm tám mươi chín</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm tám mươi chín</p>`;
                  break;
              }
              break;
          }
        }
        break;
      default:
        if (units == 0) {
          switch (hundreds) {
            case 1:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Một trăm chín mươi</p>`;
              break;
            case 2:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Hai trăm chín mươi</p>`;
              break;
            case 3:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Ba trăm chín mươi</p>`;
              break;
            case 4:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bốn trăm chín mươi</p>`;
              break;
            case 5:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Năm trăm chín mươi</p>`;
              break;
            case 6:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Sáu trăm chín mươi</p>`;
              break;
            case 7:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Bảy trăm chín mươi</p>`;
              break;
            case 8:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Tám trăm chín mươi</p>`;
              break;
            default:
              document.getElementById(
                "result7"
              ).innerHTML = `<p>Chín trăm chín mươi</p>`;
              break;
          }
        } else {
          switch (units) {
            case 1:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm chín mươi mốt</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm chín mươi mốt</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm chín mươi mốt</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm chín mươi mốt</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm chín mươi mốt</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm chín mươi mốt</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm chín mươi mốt</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm chín mươi mốt</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm chín mươi mốt</p>`;
                  break;
              }
              break;
            case 2:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm chín mươi hai</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm chín mươi hai</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm chín mươi hai</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm chín mươi hai</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm chín mươi hai</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm chín mươi hai</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm chín mươi hai</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm chín mươi hai</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm chín mươi hai</p>`;
                  break;
              }
              break;
            case 3:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm chín mươi ba</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm chín mươi ba</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm chín mươi ba</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm chín mươi ba</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm chín mươi ba</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm chín mươi ba</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm chín mươi ba</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm chín mươi ba</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm chín mươi ba</p>`;
                  break;
              }
              break;
            case 4:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm chín mươi tư</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm chín mươi tư</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm chín mươi tư</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm chín mươi tư</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm chín mươi tư</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm chín mươi tư</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm chín mươi tư</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm chín mươi tư</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm chín mươi tư</p>`;
                  break;
              }
              break;
            case 5:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm chín mươi lăm</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm chín mươi lăm</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm chín mươi lăm</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm chín mươi lăm</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm chín mươi lăm</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm chín mươi lăm</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm chín mươi lăm</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm chín mươi lăm</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm chín mươi lăm</p>`;
                  break;
              }
              break;
            case 6:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm chín mươi sáu</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm chín mươi sáu</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm chín mươi sáu</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm chín mươi sáu</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm chín mươi sáu</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm chín mươi sáu</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm chín mươi sáu</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm chín mươi sáu</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm chín mươi sáu</p>`;
                  break;
              }
              break;
            case 7:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm chín mươi bảy</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm chín mươi bảy</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm chín mươi bảy</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm chín mươi bảy</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm chín mươi bảy</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm chín mươi bảy</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm chín mươi bảy</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm chín mươi bảy</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm chín mươi bảy</p>`;
                  break;
              }
              break;
            case 8:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm chín mươi tám</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm chín mươi tám</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm chín mươi tám</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm chín mươi tám</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm chín mươi tám</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm chín mươi tám</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm chín mươi tám</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm chín mươi tám</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm chín mươi tám</p>`;
                  break;
              }
              break;
            default:
              switch (hundreds) {
                case 1:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Một trăm chín mươi chín</p>`;
                  break;
                case 2:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Hai trăm chín mươi chín</p>`;
                  break;
                case 3:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Ba trăm chín mươi chín</p>`;
                  break;
                case 4:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bốn trăm chín mươi chín</p>`;
                  break;
                case 5:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Năm trăm chín mươi chín</p>`;
                  break;
                case 6:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Sáu trăm chín mươi chín</p>`;
                  break;
                case 7:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Bảy trăm chín mươi chín</p>`;
                  break;
                case 8:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Tám trăm chín mươi chín</p>`;
                  break;
                default:
                  document.getElementById(
                    "result7"
                  ).innerHTML = `<p>Chín trăm chín mươi chín</p>`;
                  break;
              }
              break;
          }
        }
        break;
    }
  }
}

// Exercise 8
function findOut() {
  var stu1Name = document.getElementById("name1").value;
  var x1Value = document.getElementById("number1-8").value * 1;
  var y1Value = document.getElementById("number2-8").value * 1;

  var stu2Name = document.getElementById("name2").value;
  var x2Value = document.getElementById("number3-8").value * 1;
  var y2Value = document.getElementById("number4-8").value * 1;

  var stu3Name = document.getElementById("name3").value;
  var x3Value = document.getElementById("number5-8").value * 1;
  var y3Value = document.getElementById("number6-8").value * 1;

  var xValue = document.getElementById("number7-8").value * 1;
  var yValue = document.getElementById("number8-8").value * 1;

  var stu1Dis = Math.sqrt(
    (xValue - x1Value) * (xValue - x1Value) +
      (yValue - y1Value) * (yValue - y1Value)
  );
  var stu2Dis = Math.sqrt(
    (xValue - x2Value) * (xValue - x2Value) +
      (yValue - y2Value) * (yValue - y2Value)
  );
  var stu3Dis = Math.sqrt(
    (xValue - x3Value) * (xValue - x3Value) +
      (yValue - y3Value) * (yValue - y3Value)
  );
  if (stu1Dis >= stu2Dis && stu1Dis >= stu3Dis) {
    document.getElementById(
      "result8"
    ).innerHTML = `<p>sinh viên ${stu1Name}</p>`;
  } else if (stu2Dis >= stu1Dis && stu2Dis >= stu3Dis) {
    document.getElementById(
      "result8"
    ).innerHTML = `<p>sinh viên ${stu2Name}</p>`;
  } else {
    document.getElementById(
      "result8"
    ).innerHTML = `<p>sinh viên ${stu3Name}</p>`;
  }
}
